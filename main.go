package main

import (
  "bufio"
  "fmt"
	"os"
	b64 "encoding/base64"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter String: ")
	text, _ := reader.ReadString('\n')

	sDec, _ := b64.StdEncoding.DecodeString(text)
	fmt.Println(string(sDec))
}